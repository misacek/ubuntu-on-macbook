#!/bin/bash -x

mount(){
    mount -t proc none /proc
    mount -t sysfs none /sys
    mount -t devpts none /dev/pts
}

update(){
    mv /etc/resolv.conf /etc/resolv.conf.bak
    echo 'nameserver 8.8.8.8' | tee /etc/resolv.conf

    apt-get update
    apt-get upgrade
    apt-get dist-upgrade
    apt-get autoremove

    apt-get -f install
}

mount
update
