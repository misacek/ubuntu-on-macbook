#!/bin/bash -x

extract_image(){
    sudo mkdir /tmp/iso ${MYDIR}/custom-iso

    # mount iso to /tmp/iso
    sudo mount ${ISO} /tmp/iso

    # copy content of the iso to custom-iso/
    rsync -a --exclude=casper/filesystem.squashfs /tmp/iso/ ${MYDIR}/custom-iso/

    # unsquash root filesystem from the iso to ${MYDIR}/sqashfs-root/
    sudo unsquashfs ${MYDIR}/iso/casper/filesystem.squashfs

    # umount original iso
    sudo umount iso
}

chroot_prepare(){
    # copy chroot.sh to chroot
    cp ${MYDIR}/chroot-script.sh squashfs-root/
    chmod 755 squashfs-root/chroot-script.sh

    # mount /dev to chroot
    sudo mount --bind /dev squashfs-root/dev/
}

export ISO=~/Downloads/kubuntu-16.04.4-desktop-amd64.iso
export SQASHFS_ROOT=customiso/

# extract_image
chroot_prepare
